# SSI in context

Bij Digicampus verzamelen wij diverse zaken over SSI.

1. [Een technisch overzicht](https://gitlab.com/digicampus/ssi/ssi-overview)
2. [Een wiki voor discussie](https://gitlab.com/digicampus/ssi/ssi-in-context/-/wikis/home)
3. [Een blog](https://ssi.digicampus.tech) ([broncode](https://gitlab.com/digicampus/ssi/ssi-blog))

